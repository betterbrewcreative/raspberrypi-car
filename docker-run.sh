#!/bin/bash

docker build --build-arg LOCAL_USER_ID=`id -u $USER` -t yocto-raspberrypi-car .
docker run -it \
    -v ~/proj/raspberrypi-car/raspberrypi-car:/home/rpcar/raspberrypi-car \
    yocto-raspberrypi-car
