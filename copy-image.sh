#!/bin/bash

echo "Unzipping wic"
bzip2 -d -f /home/chris/proj/raspberrypi-car/raspberrypi-car/tmp/deploy/images/raspberrypi4-64/raspberrypi-car-image-raspberrypi4-64.wic.bz2

echo "Copying unzipped wic to this directory"
cp /home/chris/proj/raspberrypi-car/raspberrypi-car/tmp/deploy/images/raspberrypi4-64/raspberrypi-car-image-raspberrypi4-64.wic .

