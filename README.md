Docker:

The Dockerfile creates a containter that can be used to build a linux distro for the raspberry pi car project.
Once the container is running first source oe-init-build-env for the project
    
    poky>source oe-init-build-env ~/raspberrypi-car/

The linux image can then be build using

    >bitbake raspberrypi-car-image

Once the distro is built the create-bootable-sd.sh can be used to create an sd card that will boot.

