FROM ubuntu:20.04

RUN apt-get update && \
        DEBIAN_FRONTEND="noninteractive" apt-get install -y \
        gawk \
        wget \
        git-core \
        diffstat \
        unzip \
        texinfo \
        gcc-multilib \
        build-essential \
        chrpath \
        socat \
        cpio \
        python3 \
        python3-pip \
        python3-pexpect \
        xz-utils \
        debianutils \
        iputils-ping \
        python3-git \
        python3-jinja2 \
        libegl1-mesa \
        libsdl1.2-dev \
        pylint3 \
        xterm \
        python3-subunit \
        mesa-common-dev \
        vim \
        locales \
        libboost1.71-all-dev

RUN sed -i '/en_US.UTF-8/s/^# //g' /etc/locale.gen && \
    locale-gen

ARG LOCAL_USER_ID

RUN adduser -u ${LOCAL_USER_ID} --disabled-password --gecos '' rpcar

USER rpcar
ENV HOME /home/rpcar
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
WORKDIR /home/rpcar

RUN git clone git://git.yoctoproject.org/poky.git
RUN git clone git://git.yoctoproject.org/meta-raspberrypi
RUN git clone git://git.openembedded.org/meta-openembedded

WORKDIR /home/rpcar/poky
RUN git checkout hardknott

WORKDIR /home/rpcar/meta-raspberrypi
RUN git checkout hardknott

WORKDIR /home/rpcar/meta-openembedded
RUN git checkout hardknott

WORKDIR /home/rpcar

