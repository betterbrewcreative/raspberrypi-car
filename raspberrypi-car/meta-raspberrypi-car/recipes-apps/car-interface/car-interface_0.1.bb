DESCRIPTION = "Interface to car application for raspberry pi"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

DEPENDS = "boost"

inherit cmake

SRC_URI = "git://bitbucket.org/betterbrewcreative/raspberrypi-car-interface-source.git;protocol=https"

SRCREV = "${AUTOREV}"
S = "${WORKDIR}/git"

EXTRA_OECMAKE += " -DBUILD_TARGET=1"
