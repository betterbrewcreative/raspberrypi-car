DESCRIPTION = "Car application for raspberry pi"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit update-rc.d

SRC_URI = "file://car_service"

do_install() {
    install -d ${D}${sysconfdir}/init.d
    install -m 0755 ${WORKDIR}/car_service ${D}${sysconfdir}/init.d
}

INITSCRIPT_NAME = "car_service"
INITSCRIPT_PARAMS = "start 99 1 2 3 4 5 . stop 20 0 1 6 ."
RDEPENDS_${PN} = "initscripts car"
CONFFILES_${PN} += "${sysconfdir}/init.d/car_service"

