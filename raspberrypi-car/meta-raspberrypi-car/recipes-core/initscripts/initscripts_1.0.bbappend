FILESEXTRAPATHS_append := ":${THISDIR}/${PN}-${PV}"
SRC_URI_append = " file://setup-wifi.sh"

do_install_append() {
    install -m 0755 ${WORKDIR}/setup-wifi.sh ${D}${sysconfdir}/init.d
    update-rc.d -r ${D} setup-wifi.sh start 99 2 3 4 5 .
}

MASKED_SCRIPTS = " \
setup_wifi \
"
