#!/bin/bash

if [ "$#" -ne 1 ] ; then
    echo "Must specify device (/dev/sda)"
    exit 1
fi

echo "Unmounting /media/chris/root"
umount /media/chris/root

echo "Unmounting /media/chris/boot"
umount /media/chris/boot

./copy-image.sh

echo "Creating bootable image on $1"
sudo dd if=raspberrypi-car-image-raspberrypi4-64.wic of=$1 bs=4096

echo "Syncing"
sync

echo "Unmounting /media/chris/root"
umount /media/chris/root

echo "Unmounting /media/chris/boot"
umount /media/chris/boot


